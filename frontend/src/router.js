import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import SignUpPage from "./views/SignUpPage.vue";
import SettingsPage from "./views/SettingsPage.vue";
import RepoPage from "./views/RepoPage.vue";
import CommitsPage from "./views/CommitsPage.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/signup",
      name: "signup",
      component: SignUpPage
    },
    {
      path: "/settings",
      name: "settings",
      component: SettingsPage
    },
    {
      path: "/repos",
      name: "repo",
      component: RepoPage
    },
    {
      path: "/commits/:repoName",
      name: "commits",
      component: CommitsPage
    }
  ]
});
