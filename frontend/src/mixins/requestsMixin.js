const APIURL = "http://localhost:3000";
const axios = require("axios");

axios.interceptors.request.use(
  config => {
    config.headers.authorization = localStorage.getItem("token");
    return config;
  },
  error => Promise.reject(error)
);

axios.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    if (error.response.status == 401) {
      localStorage.clear();
    }
    return Promise.reject(error);
  }
);

export const requestsMixin = {
  methods: {
    signUp(data) {
      return axios.post(`${APIURL}/users/signup`, data);
    },

    logIn(data) {
      return axios.post(`${APIURL}/users/login`, data);
    },

    changePassword(data) {
      return axios.post(`${APIURL}/users/changePassword`, data);
    },

    currentUser() {
      return axios.get(`${APIURL}/users/currentUser`);
    },

    setBitbucketCredentials(data) {
      return axios.post(`${APIURL}/bitbucket/setBitbucketCredentials`, data);
    },

    repos(page) {
      return axios.get(`${APIURL}/bitbucket/repos/${page || 1}`);
    },

    commits(repoName) {
      return axios.get(`${APIURL}/bitbucket/commits/${repoName}`);
    }
  }
};
